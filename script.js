// NodeJS File-System reader
const fs = require('fs');
fs.readFile('input.txt', 'ascii', function (err, data) {
  if (err) {
    return console.log(err)
  }

  const telemetryData = parsingTxtFile(data);
  violationAlerts(telemetryData)
})


// parsing data
const parsingTxtFile = (data => {
  // Splitting txt file into an array
  const arr = data.split('\n')
  let arrOfObj = []

  // splitting each telemetry reading into their own array of strings
  arr.forEach(telemetryReading => {
    const readingsArr = telemetryReading.split('\|')

    // formatting date
    let dateNoSpace = readingsArr[0].split(/\s/).join('')
    let ISOTime = dateNoSpace.slice(0, 4) + "-" + dateNoSpace.slice(4, 6) + "-" + dateNoSpace.slice(6, 8) + "T" + dateNoSpace.slice(8) + "Z"

    // converting telemetry array to object
    let reading = {}
    reading.timestamp = ISOTime
    reading.satelliteId = Number(readingsArr[1])
    reading.redHigh = Number(readingsArr[2])
    reading.yellowHigh = Number(readingsArr[3])
    reading.yellowLow = Number(readingsArr[4])
    reading.redLow = Number(readingsArr[5])
    reading.rawValue = Number(readingsArr[6])
    reading.component = readingsArr[7]

    //pushing each telemetry reading object to an array
    arrOfObj.push(reading)
  })
  return arrOfObj
})


// violations pushed into array
let violations = []
let alertArr = []


// violation alerts based on thermal and voltage conditions
let violationAlerts = (data => {
  // checking conditions for each telemetry reading
  data.forEach(telemetryReading => {
    let reading = {}

    // battery voltage readings that are under the red low limit
    if (telemetryReading.rawValue < telemetryReading.redLow && telemetryReading.component === 'BATT') {
      reading.satelliteId = telemetryReading.satelliteId
      reading.severity = "RED LOW"
      reading.component = telemetryReading.component
      reading.timestamp = telemetryReading.timestamp

      // thermostat readings that exceed the red high limit
    } else if (telemetryReading.rawValue > telemetryReading.redHigh && telemetryReading.component === 'TSTAT') {
      reading.satelliteId = telemetryReading.satelliteId
      reading.severity = "RED HIGH"
      reading.component = telemetryReading.component
      reading.timestamp = telemetryReading.timestamp

    }
    violations.push(reading)
  })

  extractAlertWithThreeOccur('TSTAT')
  extractAlertWithThreeOccur('BATT')

  console.log(alertArr)
})


// first instance of alerts with three occurrences
const extractAlertWithThreeOccur = (component) => {
  // grouped arr of alerts with matching satelliteId and component
  const groupedArr = violations.filter(violation => violation.satelliteId && violation.component === component)

  // accumulates occurence of the alerts within the groupedArr
  const alertOccurrence = groupedArr.reduce((acc, o) => (acc[o.satelliteId] = (acc[o.satelliteId] || 0) + 1, acc), {})

  // loop that matches key with value of 3 alerted occurrences with the alerts in groupedArr to find the first occur and pushes to alertArr
  for (let key in alertOccurrence) {
    if (alertOccurrence.hasOwnProperty(key)) {
      if (alertOccurrence[key] == 3) {
        alertObj = groupedArr.find(reading => reading.satelliteId == key.toString())

        alertArr.push(alertObj)
      }
    }
  }
}